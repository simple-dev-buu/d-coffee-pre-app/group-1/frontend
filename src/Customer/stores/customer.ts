import { defineStore } from 'pinia'
import { computed, reactive, ref } from 'vue'
import type { Customer } from '../types/customer'
import CustomerService from '../services/customer'

export const useCustomerStore = defineStore('customer', () => {
  //   const loadingStore = useLoadingStore()
  const service = new CustomerService()
  const dialogState = ref(false)
  const tempItem = reactive<Customer>({
    name: '',
    telephone: '',
    birthDay: new Date('2004-28-05')
  })
  const titleDialog = computed(() => (tempItem.id ? 'Edit' : 'New'))
  const items = ref<Customer[]>([])
  const currentMember = ref<Customer | null>()
  const headers = [
    {
      title: 'ID',
      key: 'id'
    },
    {
      title: 'Name',
      key: 'name'
    },
    {
      title: 'Telephone',
      key: 'telephone'
    },
    {
      title: 'Register Day',
      key: 'registerDate'
    },
    {
      title: 'Birth Day',
      key: 'birthDay'
    },
    {
      title: 'Point',
      key: 'point'
    },
    {
      title: 'Actions',
      key: 'actions',
      sortable: false
    }
  ]

  const openDialog = () => {
    dialogState.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialogState.value = false
  }
  const save = async () => {
    if (tempItem.id) {
      await service.update(tempItem)
    } else {
      await service.save(tempItem)
    }
    closeDialog()
    getAll()
  }
  const resetItem = () => {
    tempItem.name = ''
    tempItem.telephone = ''
    tempItem.birthDay = new Date('2004-28-05')
  }
  const getAll = async () => {
    // loadingStore.doLoad()
    try {
      items.value = await service.getAll()
    } catch (e: any) {
      console.log(e)
    }
    // loadingStore.finishLoad()
  }
  const editItem = (item: Customer) => {
    const tempDateString = item.birthDay.toString()
    tempItem.id = item.id
    tempItem.telephone = item.telephone
    tempItem.name = item.name
    tempItem.birthDay = new Date(tempDateString)
    openDialog()
  }
  const deleteItem = async (item: Customer) => {
    if (item.id) {
      await service.delete(item.id)
      getAll()
    }
  }
  return {
    titleDialog,
    editItem,
    deleteItem,
    headers,
    items,
    customerItem: tempItem,
    dialogState,
    openDialog,
    closeDialog,
    save,
    resetItem,
    getAll
  }
})
