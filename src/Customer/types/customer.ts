export type Customer = {
  id?: number
  name: string
  telephone: string
  registerDate?: Date
  birthDay: Date
  point?: number
}
