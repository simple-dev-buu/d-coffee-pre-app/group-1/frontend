import http from '@/services/http'
import type { Customer } from '../types/customer'

class CustomerService {
  public async save(item: Customer) {
    await http.post('/customers', item)
  }
  public async update(item: Customer) {
    await http.patch(`/customers/${item.id}`, item)
  }
  public async getAll() {
    const res = await http.get('/customers')
    return res.data
  }

  public async getByTel(tel: string): Promise<Customer> {
    const res = await http.get('/customers/telephone/' + tel)
    return res.data
  }

  public async delete(index: number) {
    await http.delete(`/customers/${index}`)
  }
}

export default CustomerService
