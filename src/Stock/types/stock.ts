type Status = 'L' | 'M'
type Stock = {
  id?: number
  code: string
  name: string
  price: number
  balance: number
  unit: number
  status: Status[]
}

export type { Stock, Status }
