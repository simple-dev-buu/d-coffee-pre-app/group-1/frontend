import { defineStore } from 'pinia'
// import { useLoadingStore } from './loading'
import StockService from '@/Stock/services/stock'
import { reactive, ref } from 'vue'
import type { Stock } from '@/Stock/types/stock'

export const useStockStore = defineStore('stock', () => {
  //   const loadingStore = useLoadingStore()
  const stockService = new StockService()
  const dialog = ref(false)
  const showCard = ref(false)
  const stockItem = reactive<Stock>({
    code: '',
    name: '',
    price: 0,
    balance: 0,
    unit: 0,
    status: []
  })
  const items = ref<Stock[]>([])
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Code', key: 'code', sortable: false },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Price', key: 'price', sortable: false },
    { title: 'Balance', key: 'balance', sortable: false },
    { title: 'Amount', key: 'amount', sortable: false },
    { title: 'Status', key: 'status', sortable: false },
    { title: 'Actions', key: 'actions', sortable: false }
  ]

  const openDialog = () => {
    // resetItem()
    dialog.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }
  const openEdit = () => {
    showCard.value = !showCard.value
  }
  const save = async () => {
    updateStatus()
    await stockService.save(stockItem)
    console.log(stockItem.status)
    closeDialog()
    getAll()
  }
  const resetItem = () => {
    stockItem.code = ''
    stockItem.name = ''
    stockItem.price = 0
    stockItem.balance = 0
    stockItem.unit = 0
    stockItem.status = []
  }

  function updateStatus() {
    if (stockItem.balance <= 5) {
      stockItem.status = 'L'
    } else {
      stockItem.status = 'M'
    }
  }
  // function addReceiptItem(ingredient: Stock) {
  // const index = items.value.findIndex((item) => item.ingredient?.id === ingredient.id)
  // if (index >= 0) {
  //   items.value[index].balance++
  //   // calReceipt()
  //   return
  // } else {
  //   const newReceipt: Stock = {
  //     id: lastIdReceiptItem++,
  //     name: product.name,
  //     price: product.price,
  //     unit: 1,
  //     productId: product.id,
  //     product: product
  //   }
  //   receiptItems.value.push(newReceipt)
  //   calReceipt()
  // }

  function removeReceiptItem(receiptItem: Stock) {
    const index = items.value.findIndex((item) => item === receiptItem)
    items.value.splice(index, 1)
    // calReceipt()
  }

  function inc(item: Stock) {
    item.balance++
    // calReceipt()
  }
  function dec(item: Stock) {
    if (item.balance === 1) {
      removeReceiptItem(item)
    }
    item.balance--
    // calReceipt()
  }

  const getAll = async () => {
    // loadingStore.doLoad()
    items.value = await stockService.getAll()
    // loadingStore.finishLoad()
    // console.log(items.value)
  }
  function getItemAll(item: Stock) {
    console.log(item)
  }
  const editItem = (item: Stock) => {
    stockItem.id = item.id
    stockItem.code = item.code
    stockItem.name = item.name
    stockItem.price = item.price
    stockItem.balance = item.balance
    stockItem.unit = item.unit
    stockItem.status = item.status
    console.log(stockItem.status)
    // openDialog()
    openEdit()
  }
  const deleteItem = async (item: Stock) => {
    if (item.id) {
      await stockService.delete(item.id)
      getAll()
    }
  }

  return {
    headers,
    items,
    stockItem,
    dialog,
    updateStatus,
    openEdit,
    editItem,
    deleteItem,
    openDialog,
    closeDialog,
    save,
    resetItem,
    getAll,
    getItemAll,
    inc,
    dec,
    removeReceiptItem
  }
})
