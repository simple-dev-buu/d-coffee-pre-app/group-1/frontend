import type { User } from '@/User/types/Users'

type TypeWork = 'Missing' | 'Normal' | 'Normal/OT'
type CheckTime = {
  id: number
  date: string
  user: User | null
  clockIn: string
  clockOut: string
  timeWorked: number
  typeWork: TypeWork | null
}

export type { TypeWork, CheckTime }
