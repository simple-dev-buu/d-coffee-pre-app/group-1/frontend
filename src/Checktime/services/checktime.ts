import type { CheckTime } from '@/Checktime/types/checktime'
import http from '../../services/http'

class CheckTimeService {
  public async save(item: CheckTime) {
    await http.post('/checktime', item)
  }
  public async getAll() {
    const res = await http.get('/checktime')
    return res.data
  }
  public async delete(index: number) {
    await http.delete(`/checktime/${index}`)
  }
}

export default CheckTimeService
