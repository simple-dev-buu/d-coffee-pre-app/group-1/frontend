import { defineStore } from 'pinia'
// import { useLoadingStore } from './loading'
import CheckTimeService from '@/Checktime/services/checktime'
import { reactive, ref } from 'vue'
import type { CheckTime } from '@/Checktime/types/checktime'

export const useChecktimesStore = defineStore('checktime', () => {
  //   const loadingStore = useLoadingStore()
  const checktimeService = new CheckTimeService()
  const dialog = ref(false)
  const checktimeItem = reactive<CheckTime>({
    id: -1,
    date: '',
    user: null,
    clockIn: '',
    clockOut: '',
    timeWorked: 0,
    typeWork: null
  })
  const chItem = ref<CheckTime[]>([])
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Date', key: 'date' },
    { title: 'User', key: 'user', sortable: false },
    { title: 'Employee', key: 'employee', sortable: false },
    { title: 'Clock In', key: 'clockIn' },
    { title: 'Clock Out', key: 'clockOut' },
    { title: 'Time Worked', key: 'timeWorked', sortable: false },
    { title: 'Types', key: 'types', sortable: false }
  ]
  const openDialog = () => {
    dialog.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }
  const save = async () => {
    await checktimeService.save(checktimeItem)
    closeDialog()
    getAll()
  }
  const resetItem = () => {
    checktimeItem.date = ''
    checktimeItem.user = null
    checktimeItem.clockIn = ''
    checktimeItem.clockOut = ''
    checktimeItem.timeWorked = 0
    checktimeItem.typeWork = null
  }
  const getAll = async () => {
    // loadingStore.doLoad()
    chItem.value = await checktimeService.getAll()
    // loadingStore.finishLoad()
  }
  const editItem = (item: CheckTime) => {
    checktimeItem.id = item.id
    checktimeItem.date = item.date
    checktimeItem.user = item.user
    checktimeItem.clockIn = item.clockIn
    checktimeItem.clockOut = item.clockOut
    checktimeItem.timeWorked = item.timeWorked
    checktimeItem.typeWork = item.typeWork
    console.log(checktimeItem.typeWork)
    openDialog()
  }
  const deleteItem = async (item: CheckTime) => {
    if (item.id) {
      await checktimeService.delete(item.id)
      getAll()
    }
  }

  return {
    headers,
    chItem,
    checktimeItem,
    dialog,
    editItem,
    deleteItem,
    openDialog,
    closeDialog,
    save,
    resetItem,
    getAll
  }
})
