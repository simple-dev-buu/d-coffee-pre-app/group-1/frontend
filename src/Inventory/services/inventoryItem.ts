import http from '@/services/http'
import type { Inventory } from '../types/inventory'
import type { InventoryItem } from '../types/inventoryItem'

class InventoryItemService {
  public async save(item: InventoryItem) {
    await http.post('/inventoryItem', item)
  }
  public async getAll() {
    const res = await http.get('/inventoryItem')
    return res.data
  }
  public async delete(index: number) {
    await http.delete(`/inventoryItem/${index}`)
  }
}

export default InventoryItemService
