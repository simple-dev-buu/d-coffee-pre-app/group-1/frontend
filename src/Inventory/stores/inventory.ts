import { defineStore } from 'pinia'

import { reactive, ref } from 'vue'
import type { InventoryItem } from '../types/inventoryItem'
import type { Ingredient } from '../types/ingredient'
import InventoryItemService from '../services/inventoryItem'

export const useInventoryStore = defineStore('inventory', () => {
  //   const loadingStore = useLoadingStore()
  const inventoryItemService = new InventoryItemService()
  const dialog = ref(false)
  const inventoryItem = reactive<InventoryItem>({
    id: 0,
    balance: 0,
    minBalance: 0,
    value: 0,
    inventoryId: 0,
    ingredientId: 0
  })
  const items = ref<InventoryItem[]>([])
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Balance', key: 'balance', sortable: false },
    { title: 'Min Balance', key: 'minBalance', sortable: false },
    { title: 'Unit', key: 'unit', sortable: false },
    { title: 'Actions', key: 'actions', sortable: false }
  ]

  const openDialog = () => {
    dialog.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialog.value = false
  }
  const save = async () => {
    await inventoryItemService.save(inventoryItem)
    console.log(inventoryItem)
    closeDialog()
    getAll()
  }
  const resetItem = () => {
    inventoryItem.id = 0
    inventoryItem.balance = 0
    inventoryItem.minBalance = 0
    inventoryItem.ingredientId = 0
  }

  const getAll = async () => {
    // loadingStore.doLoad()
    items.value = await inventoryItemService.getAll()
    // loadingStore.finishLoad()
    // console.log(items.value)
  }
  const editItem = (item: InventoryItem) => {
    inventoryItem.id = item.id
    inventoryItem.balance = item.balance
    inventoryItem.minBalance = item.minBalance

    openDialog()
  }
  const deleteItem = async (item: InventoryItem) => {
    if (item.id) {
      await inventoryItemService.delete(item.id)
      getAll()
    }
  }

  return {
    headers,
    items,
    inventoryItem,
    dialog,
    editItem,
    deleteItem,
    openDialog,
    closeDialog,
    save,
    resetItem,
    getAll
  }
})
