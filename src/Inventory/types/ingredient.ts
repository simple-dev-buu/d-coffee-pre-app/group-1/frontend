type Ingredient = {
  id: number
  name: string
  unit: string
}
export { type Ingredient }
