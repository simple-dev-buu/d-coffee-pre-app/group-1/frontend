type Inventory = {
  id: number
  totalValue: number
}
export { type Inventory }
