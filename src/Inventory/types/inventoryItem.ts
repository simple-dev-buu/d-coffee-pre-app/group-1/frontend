import type { Ingredient } from './ingredient'
import type { Inventory } from './inventory'

type InventoryItem = {
  id: number
  balance: number
  minBalance: number
  value: number
  inventoryId: number
  inventory?: Inventory
  ingredientId: number
  ingredient?: Ingredient
}
export { type InventoryItem }
