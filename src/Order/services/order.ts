import type { Receipt } from '@/Pos/types/Receipt'
import type { ReceiptItem } from '@/Pos/types/ReceiptItem'
import http from '@/services/http'

type ReceiptDto = {
  orderItems: {
    productId: number
    qty: number
  }[]
  userId: number
  customerId: number
}

function addOrder(receipt: Receipt, receiptItems: ReceiptItem[]) {
  const receiptDto: ReceiptDto = {
    orderItems: [],
    userId: -1,
    customerId: -1
  }
  receiptDto.userId = receipt.userId
  receiptDto.customerId = receipt.memberId
  receiptDto.orderItems = receiptItems.map((item) => {
    return {
      productId: item.productId,
      qty: item.unit
    }
  })

  console.log(receiptDto)
  console.log(receiptDto.customerId)
  return http.post('/orders', receiptDto)
}

export default { addOrder }
