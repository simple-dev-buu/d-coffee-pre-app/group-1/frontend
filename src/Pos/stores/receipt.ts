import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/Pos/types/ReceiptItem'
import type { Receipt } from '@/Pos/types/Receipt'
import { useAuthStore } from '@/views/Auth/stores/auth'
import orderService from '@/Order/services/order'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import { useMemberStore } from './member'

export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const memberStore = useMemberStore()
  const receiptItems = ref<ReceiptItem[]>([])
  const receipt = ref<Receipt>()
  const receiptDialog = ref(false)
  initReceipt()
  function initReceipt() {
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      total: 0, //baht
      amount: 0, //unit
      change: 0,
      paymentType: 'Cash',
      userId: authStore.getCurrentUser()!.id!,
      memberId: -1,
      user: authStore.getCurrentUser()!
    }
    receiptItems.value = []
  }
  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    { deep: true }
  )

  const calReceipt = function () {
    receipt.value!.total = 0
    receipt.value!.amount = 0
    for (let i = 0; i < receiptItems.value.length; i++) {
      receipt.value!.total += receiptItems.value[i].price * receiptItems.value[i].unit
      receipt.value!.amount += receiptItems.value[i].unit
    }
  }

  const addReceiptItem = (newReceiptItem: ReceiptItem) => {
    receiptItems.value.push(newReceiptItem)
  }
  const deleteReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
  }
  const incUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit++
  }
  const decUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit--
    if (selectedItem.unit === 0) {
      deleteReceiptItem(selectedItem)
    }
  }
  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
  }
  const order = async () => {
    try {
      loadingStore.doLoad()
      if (memberStore.currentMember?.id) {
        receipt.value!.memberId = memberStore.currentMember?.id
      } else {
        receipt.value!.memberId = -1
      }
      console.log(receipt.value?.memberId)
      await orderService.addOrder(receipt.value!, receiptItems.value)
      initReceipt()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
      memberStore.clear()
    }
  }
  const clear = function () {
    receiptItems.value = []
    receipt.value = Object.assign({
      id: 0,
      createdDate: new Date(),
      total: 0, //baht
      amount: 0, //unit
      change: 0,
      paymentType: 'Cash',
      userId: 0,
      memberId: 0,
      user: 0

      // id: 0,
      // createDate: new Date(),
      // totalUnit: 0,
      // totalBefore: 0,
      // discount: 0,
      // netPrice: 0,
      // receivedAmount: 0,
      // change: 0,
      // promotionId: 0,
      // paymentType: 'Cash',
      // userId: 0,
      // memberId: 0,
      // givePoint: 0,
      // usePoint: 0,
      // totalPoint: 0
    })
  }

  function toEnablePromotion() {
    receiptItems.value.forEach((item) => {
      item.productId
    })
  }

  const errorInput = ref(false)
  const paymentType = ref('Cash')
  let queue = 1
  const showReceiptDialog = function () {
    receipt.value!.paymentType = paymentType.value
    console.log(queue)
    queue = queue + 1
    console.log(queue)
    if (
      receipt.value!.change >= 0 &&
      receipt.value!.total > 0 &&
      receipt.value!.paymentType === 'Cash' &&
      receiptItems.value.length > 0
    ) {
      errorInput.value = false
      receiptDialog.value = true
      receipt.value!.receiptItems = receiptItems.value
      receipt.value!.paymentType = paymentType.value
      // }else if(receipt.value!.paymentType === 'PromptPay') {
      //   dialogPromptPay.value = true
      //   receipt.value!.total = receipt.value.netPrice
      //   receipt.value!.receiptItem = receiptItems.value
      //   receipt.value!.paymentType = paymentType.value
      // }else if(receipt.value.paymentType === 'Credit Card') {
      //   dialogCredit.value = true
      //   receipt.value.receivedAmount = receipt.value.netPrice
      //   receipt.value.receiptItem = receiptItems.value
      //   receipt.value.paymentType = paymentType.value
      // }
      // else {
      //   errorInputPoint.value = true
    }
  }
  return {
    receipt,
    receiptItems,
    receiptDialog,
    clear,
    addReceiptItem,
    incUnitOfReceiptItem,
    decUnitOfReceiptItem,
    deleteReceiptItem,
    removeItem,
    order,
    showReceiptDialog
  }
})
