// type Status = 'active' | 'inactive'
type Promotion = {
  id?: number
  name: string
  description: string
  discount: number
  combo: boolean
  startDate: Date
  endDate: Date
  status: boolean
}

export type { Promotion }
