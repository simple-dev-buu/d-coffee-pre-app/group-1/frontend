import type { Salary } from '@/Salary/types/salary'
import http from '../../services/http'

class SalaryService {
  public async save(item: Salary) {
    await http.post('/salary', item)
  }
  public async getAll() {
    const res = await http.get('/salary')
    return res.data
  }
  public async delete(index: number) {
    await http.delete(`/salary/${index}`)
  }
}

export default SalaryService
