type Status = 'รอชำระ' | 'ชำระแล้ว'
type Role = 'admin' | 'user'
type Salary = {
  id?: number
  date: string
  period: string
  employeeID: number
  roles: Role
  name: string
  timeWorked: number
  defaultSalary: number
  lossSalary: number
  netSalary: number
  state: Status
}
export type { Salary, Status, Role }
