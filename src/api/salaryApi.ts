import type { Salary } from '@/Salary/types/salary'

export const slipRecords = <Salary[]>[
  {
    id: 1,
    date: '31/01/2022',
    period: '01/01/2022 - 31/01/2022',
    employeeID: 4,
    roles: 'พนักงาน',
    name: 'Wade Wilson',
    timeWorked: 8,
    defaultSalary: 15000,
    lossSalary: 3000,
    netSalary: 12000,
    state: 'ชำระแล้ว'
  },
  {
    id: 2,
    date: '31/01/2022',
    period: '01/01/2022 - 31/01/2022',
    employeeID: 5,
    roles: 'พนักงาน',
    name: 'Billy Batson',
    timeWorked: 8,
    defaultSalary: 15000,
    lossSalary: 3000,
    netSalary: 12000,
    state: 'ชำระแล้ว'
  },
  {
    id: 3,
    date: '31/01/2022',
    period: '01/01/2022 - 31/01/2022',
    employeeID: 8,
    roles: 'พนักงาน',
    name: 'Glenn Rhee',
    timeWorked: 8,
    defaultSalary: 15000,
    lossSalary: 0,
    netSalary: 15000,
    state: 'ชำระแล้ว'
  }
]

export const pendingSlips = <Salary[]>[
  {
    id: 1,
    date: '',
    period: '01/12/2023 - 31/12/2023',
    employeeID: 4,
    roles: 'พนักงาน',
    name: 'Wade Wilson',
    timeWorked: 8,
    defaultSalary: 15000,
    lossSalary: 0,
    netSalary: 15000,
    state: 'รอชำระ'
  },
  {
    id: 2,
    date: '',
    period: '01/01/2024 - 31/01/2024',
    employeeID: 5,
    roles: 'พนักงาน',
    name: 'Billy Batson',
    timeWorked: 0,
    defaultSalary: 15000,
    lossSalary: 3000,
    netSalary: 12000,
    state: 'รอชำระ'
  },
  {
    id: 3,
    date: '',
    period: '01/01/2024 - 31/01/2024',
    employeeID: 8,
    roles: 'พนักงาน',
    name: 'Glenn Rhee',
    timeWorked: 0,
    defaultSalary: 15000,
    lossSalary: 0,
    netSalary: 15000,
    state: 'รอชำระ'
  }
]
