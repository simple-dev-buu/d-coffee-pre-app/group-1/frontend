import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/Auth/views/LoginView.vue'),
      meta: {
        layout: 'FullLayout',
        requireAuth: false
      }
    },
    {
      path: '/',
      name: 'home',
      components: {
        default: () => import('../views/HomeView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/pos',
      name: 'pos',
      components: {
        default: () => import('../Pos/views/POSView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/product',
      name: 'product',
      components: {
        default: () => import('../Product/views/ProductView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/promotion',
      name: 'promotion',
      components: {
        default: () => import('../Promotion/views/PromotionView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/stock',
      name: 'stock',
      components: {
        default: () => import('../Stock/views/StockView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/inventory',
      name: 'inventory',
      components: {
        default: () => import('../Inventory/views/InventoryView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/management',
      name: 'management',
      components: {
        default: () => import('../Stock/views/ManagementView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      }
    },
    {
      path: '/customer',
      name: 'customer',
      components: {
        default: () => import('../Customer/views/CustomerView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/order',
      name: 'order',
      components: {
        default: () => import('../Order/views/OrderView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/employee',
      name: 'employee',
      components: {
        default: () => import('../Employee/views/EmployeeView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/users',
      name: 'users',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      components: {
        default: () => import('../User/views/UserView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/salary',
      name: 'salary',
      components: {
        default: () => import('../Salary/views/SalaryView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/bill',
      name: 'bill',
      components: {
        default: () => import('../Bill/views/BillView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/checktime',
      name: 'checktime',
      components: {
        default: () => import('../Checktime/view/Checktime.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/branch',
      name: 'branch',
      components: {
        default: () => import('../Branch/view/BranchView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/MainAppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('@/views/Auth/views/LoginView.vue'),
      meta: {
        layout: 'FullLayout',
        requireAuth: false
      }
    }
  ]
})
function isLogin() {
  const user = localStorage.getItem('user')
  if (user) {
    return true
  }
  return false
}

router.beforeEach((to, from) => {
  console.log(from)
  console.log(to)
  if (to.meta.requireAuth && !isLogin()) {
    router.replace('/login')
  }
})
export default router
