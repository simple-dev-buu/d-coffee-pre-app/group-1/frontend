import http from '@/services/http'
import type { Branch } from '../types/branch'

class BranchService {
  public async save(item: Branch) {
    await http.post('/branches', item)
  }
  public async update(item: Branch) {
    await http.patch(`/branches/${item.id}`, item)
  }
  public async getAll() {
    const res = await http.get('/branches')
    return res.data
  }
  public async delete(index: number) {
    await http.delete(`/branches/${index}`)
  }
}

export default BranchService
