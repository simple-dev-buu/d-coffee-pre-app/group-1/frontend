import { defineStore } from 'pinia'
import { computed, reactive, ref } from 'vue'
import type { Branch } from '../types/branch'
import BranchService from '../services/branch'

export const useBranchStore = defineStore('branch', () => {
  //   const loadingStore = useLoadingStore()
  const service = new BranchService()
  const dialogState = ref(false)
  const tempItem = reactive<Branch>({
    name: '',
    location: ''
  })
  const titleDialog = computed(() => (tempItem.id ? 'Edit' : 'New'))
  const items = ref<Branch[]>([])
  const headers = [
    {
      title: 'ID',
      key: 'id'
    },
    {
      title: 'Name',
      key: 'name'
    },
    {
      title: 'Location',
      key: 'location'
    },
    {
      title: 'Create Day',
      key: 'createdDate'
    },
    {
      title: 'Actions',
      key: 'actions',
      sortable: false
    }
  ]

  const openDialog = () => {
    dialogState.value = true
  }
  const closeDialog = () => {
    resetItem()
    dialogState.value = false
  }
  const save = async () => {
    if (tempItem.id) {
      await service.update(tempItem)
    } else {
      await service.save(tempItem)
    }
    closeDialog()
    getAll()
  }
  const resetItem = () => {
    tempItem.name = ''
    tempItem.location = ''
  }
  const getAll = async () => {
    // loadingStore.doLoad()
    try {
      items.value = await service.getAll()
    } catch (e: any) {
      console.log(e)
    }
    // loadingStore.finishLoad()
  }
  const editItem = (item: Branch) => {
    tempItem.id = item.id
    tempItem.location = item.location
    tempItem.name = item.name
    openDialog()
  }
  const deleteItem = async (item: Branch) => {
    if (item.id) {
      await service.delete(item.id)
      getAll()
    }
  }
  return {
    titleDialog,
    editItem,
    deleteItem,
    headers,
    items,
    branchItem: tempItem,
    dialogState,
    openDialog,
    closeDialog,
    save,
    resetItem,
    getAll
  }
})
