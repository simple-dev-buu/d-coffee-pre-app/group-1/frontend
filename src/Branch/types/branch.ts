export type Branch = {
  id?: number
  name: string
  location: string
  createdDate?: Date
  status?: boolean
}
